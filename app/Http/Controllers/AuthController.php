<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('formsignup');
    }
    
    public function welcome() {
        return view('greeting');
    }

    public function daftar() {
        return view('formsignup');
    }

    public function kirim(Request $request) {
        $name = $request->nama;
        // dd($nama);
        $email = $request->email;
        return view('greeting', compact('name', 'email'));
    }    
}
